import { injectable } from "tsyringe";
import state from "../Datasource";
import NotEnoughCreditsError from "../exception/NotEnoughCreditsError";
import { PlayerState, PlayerStateResponse, RollState } from "../model/GameState";
import { roll, shouldReroll } from "../utils/HouseUtils";


@injectable()
export default class Service {

    constructor() { }

    roll = async (playerState: PlayerState): Promise<{rollState: RollState, playerState: PlayerStateResponse}> => {

        if (state.player.credits > 0) {
            const fairGameLimit = +(process.env.FAIR_GAME_LIMIT || 0);
            const midCheatLimit = +(process.env.MID_CHEAT_LIMIT || 0);

            let rollState: RollState = roll(playerState);

            if (state.player.credits > midCheatLimit) {
                if (rollState.isWinningRoll && shouldReroll(60)) {
                    console.log(`[Server] Rerolling for player wtih more than 60 credits ..`)
                    rollState = roll(playerState);
                }
            } else if (state.player.credits > fairGameLimit) {
                if (rollState.isWinningRoll && shouldReroll(30)) {
                    console.log(`[Server] Rerolling for player wtih more than 40 - 59 credits ..`)
                    rollState = roll(playerState);
                }
            }

            if (rollState.isWinningRoll) {
                const rewardToBeAdded = rollState.currentRoll[0].reward;
                state.player.credits += rewardToBeAdded;
            }

            console.log(`[Server] Roll has ended with ${state.player.credits} remaining...`)

            return {
                rollState: rollState,
                playerState: {
                    credits: state.player.credits
                }
            };
        }

        throw new NotEnoughCreditsError('Player has 0 credits remaining.');
    }

    cashOut = async () => {
        console.log(`Player is cashing out...`)
    }

    start = async (): Promise<PlayerStateResponse> => {
        const initialState: PlayerStateResponse = {
            credits: state.player.credits
        }

        return initialState;
    }





}