import { autoInjectable } from "tsyringe";
import { NextFunction, Request, Response } from 'express';
import Service from "../service/Service";


@autoInjectable()
export default class Controller {

    constructor(private service: Service) {}

    roll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            res.send(await this.service.roll({currentCredits: 4}))
        } catch (err) {
            next(err);
        }
    }

    cashOut = async (req: Request, res: Response, next: NextFunction) => {
        try {
            res.send(await this.service.cashOut())
        } catch (err) {
            next(err);
        }
    }

    start = async (req: Request, res: Response, next: NextFunction) => {
        try {
            res.send(await this.service.start())
        } catch (err) {
            next(err);
        }
    } 


}