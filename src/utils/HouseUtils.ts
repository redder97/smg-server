import state from "../Datasource";
import { PlayerState, RollState } from "../model/GameState";
import { Symbol } from "../model/GameModels";

export const roll = (playerState: PlayerState): RollState => {
    state.player.credits -= 1;

    const roll = rollForSymbols();

    const rollStateResponse: RollState = {
        currentRoll: roll,
        isWinningRoll: false
    };
    rollStateResponse['isWinningRoll'] = isWinner(rollStateResponse);

    return rollStateResponse;
}

export const shouldReroll = (chance: number): boolean => {
    const chanceForReroll: number = rng(0, 100);
    console.log(`trying reroll.. ${chanceForReroll}`);
    return chanceForReroll <= chance;
}

function rollForSymbols(): Symbol[] {
    const rolledSymbols = [];
    const symbols = state.symbols;

    for (let i = 0; i < 3; i++) {
        const rolledSymbolId = rng(0, 3);
        const rolledSymbol = symbols.filter(item => item.id == rolledSymbolId)[0];
        
        rolledSymbols.push(rolledSymbol);
    }

    return rolledSymbols;
} 

function isWinner(rollState: RollState): boolean {
    const masterSymbolId: number = rollState.currentRoll[0].id;
    
    return rollState.currentRoll.filter(symbol => symbol.id !== masterSymbolId).length === 0;
}

function rng(lowerLimit: number, upperLimit: number):number {
    return Math.floor((Math.random() * upperLimit) + lowerLimit);
}
