export default class NotEnoughCreditsError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'NotEnoughCreditsError';
    }
 }