import { NextFunction, Request, Response } from 'express';
import NotEnoughCreditsError from './NotEnoughCreditsError';



export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
    console.error(err.stack)

    if(err.name === NotEnoughCreditsError.name) {
        res.status(500).json({error: err});   
    }
}