import { Router } from "express";
import Controller from "../controller/Controller";


const router = (controller: Controller): Router => {  
    const router = Router();
    router.post('', controller.roll);
    router.post('/cash-out', controller.cashOut);
    router.get('/start', controller.start)

    return router;
}

export default router;