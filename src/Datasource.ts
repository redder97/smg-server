
//  cherry (10 credits reward), lemon (20 credits reward), orange (30 credits reward), and watermelon (40 credits reward)
import { Player, Symbol } from "./model/GameModels"

const state: State = {
    symbols: [
        {
            id: 0,
            reward: 10,
            name: 'cherry',
            label: 'C'
        },
        {
            id: 1,
            reward: 20,
            name: 'lemon',
            label: 'L'
        },
        {
            id: 2,
            reward: 30,
            name: 'orange',
            label: 'O'
        },
        {
            id: 3,
            reward: 40,
            name: 'watermelon',
            label: 'W'
        }
    ],

    player: {
        credits: 10
    }
}

export default state;

interface State {
    symbols: Symbol[],
    player: Player
}