import { Symbol } from "./GameModels";

export interface PlayerState {
}

export interface PlayerStateResponse {
    credits: number
}

export interface RollState {
    currentRoll: Symbol[]
    isWinningRoll: boolean
}


