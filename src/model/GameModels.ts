export interface Symbol {
    id: number,
    reward: number,
    name: string,
    label: string
}

export interface Player {
    credits: number
}