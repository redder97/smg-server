import 'reflect-metadata';
import dotenv  from "dotenv"
import express from 'express';


import {container} from 'tsyringe';
import Controller from './controller/Controller';
import router from './routes/Router';
import { errorHandler } from './exception/ErrorHandler';

dotenv.config();

const app = express();
const port = process.env.PORT || 3000;

app.use((req, res, next) => {

    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:33815');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
});

const controller = container.resolve(Controller);
app.use(express.json())

app.use('/roll', router(controller));



app.use(errorHandler);

app.listen(port, () => {
    console.log(`Application running with version: ${process.env.VERSION}, at port: ${port}`);
})