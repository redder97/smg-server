# SmgServer

This project is implemented in NodeJs using express framework. It uses a couple of dependencies such as tysringe to help with implementation of IoC (Inversion of Control).

## Development server

Run `npm start` for a dev server. App will be served on `http://localhost:3000/`. The app will automatically reload if you change any of the source files.



